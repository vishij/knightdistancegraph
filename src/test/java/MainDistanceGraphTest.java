import org.junit.Assert;
import org.junit.Test;

/**
 * Test class to test all the possible combinations for matrix inputs.
 * Since the problem statement assumes that initial knight coordinates will be within matrix length and width,
 * that has not been tested.
 *
 */

public class MainDistanceGraphTest {

    private int[][] expectedMatrix;
    private ChessBoardDetails chessBoardDetails;
    private MinDistanceGraph minDistanceGraph = new MinDistanceGraph();
    private int boardLength, boardWidth, initialRowCoordinate, initialColCoordinate;

    /**
     * Test case with knight position such that all the cells can be reached.
     */
    @Test
    public void testKnightMovesWithAllCellsCovered() {
        expectedMatrix = new int[4][3];
        expectedMatrix[0][0] = 0;
        expectedMatrix[0][1] = 3;
        expectedMatrix[0][2] = 2;
        expectedMatrix[1][0] = 3;
        expectedMatrix[1][1] = 4;
        expectedMatrix[1][2] = 1;
        expectedMatrix[2][0] = 2;
        expectedMatrix[2][1] = 1;
        expectedMatrix[2][2] = 4;
        expectedMatrix[3][0] = 5;
        expectedMatrix[3][1] = 2;
        expectedMatrix[3][2] = 3;
        boardLength = 3;
        boardWidth = 4;
        initialColCoordinate = 0;
        initialRowCoordinate = 0;
        chessBoardDetails = new ChessBoardDetails(boardLength, boardWidth);
        int distanceMatrix[][] = minDistanceGraph.minStepToReachTarget(initialRowCoordinate, initialColCoordinate, boardLength, boardWidth, chessBoardDetails);
        for (int i = 0; i < boardWidth; i++) {
            Assert.assertArrayEquals(expectedMatrix[i], distanceMatrix[i]);
        }
    }

    /**
     * Test case 0x0 matrix size as input
     */
    @Test(expected = IllegalArgumentException.class)
    public void testDistanceGraphWithNoMatrix() {
        boardLength = 0;
        boardWidth = 0;
        chessBoardDetails = new ChessBoardDetails(boardLength, boardWidth);
    }

    /**
     * Test case 1x1 matrix size as input
     */
    @Test
    public void testDistanceGraphWithSingleCell() {
        expectedMatrix = new int[1][1];
        expectedMatrix[0][0] = 0;
        boardLength = 1;
        boardWidth = 1;
        initialColCoordinate = 0;
        initialRowCoordinate = 0;
        chessBoardDetails = new ChessBoardDetails(boardLength, boardWidth);
        int distanceMatrix[][] = minDistanceGraph.minStepToReachTarget(initialRowCoordinate, initialColCoordinate, boardLength, boardWidth, chessBoardDetails);
        for (int i = 0; i < boardWidth; i++) {
            Assert.assertArrayEquals(expectedMatrix[i], distanceMatrix[i]);
        }
    }

    /**
     * Test case with one row and greater than one columns in the matrix
     */
    @Test
    public void testDistanceGraphMatrixWithOneRow() {
        expectedMatrix = new int[1][3];
        expectedMatrix[0][0] = -1;
        expectedMatrix[0][1] = 0;
        expectedMatrix[0][2] = -1;
        boardLength = 3;
        boardWidth = 1;
        initialColCoordinate = 1;
        initialRowCoordinate = 0;
        chessBoardDetails = new ChessBoardDetails(boardLength, boardWidth);
        int distanceMatrix[][] = minDistanceGraph.minStepToReachTarget(initialRowCoordinate, initialColCoordinate, boardLength, boardWidth, chessBoardDetails);
        for (int i = 0; i < boardWidth; i++) {
            Assert.assertArrayEquals(expectedMatrix[i], distanceMatrix[i]);
        }
    }

    /**
     * Test case with one column and greater than one row in the matrix
     */
    @Test
    public void testDistanceGraphMatrixWithOneCol() {
        expectedMatrix = new int[3][1];
        expectedMatrix[0][0] = -1;
        expectedMatrix[1][0] = 0;
        expectedMatrix[2][0] = -1;
        boardLength = 1;
        boardWidth = 3;
        initialColCoordinate = 0;
        initialRowCoordinate = 1;
        chessBoardDetails = new ChessBoardDetails(boardLength, boardWidth);
        int distanceMatrix[][] = minDistanceGraph.minStepToReachTarget(initialRowCoordinate, initialColCoordinate, boardLength, boardWidth, chessBoardDetails);
        for (int i = 0; i < boardLength; i++) {
            Assert.assertArrayEquals(expectedMatrix[i], distanceMatrix[i]);
        }
    }

    /**
     * Test case with knight's position such that no cells can be reached (except initial position)
     */
    @Test
    public void testDistanceGraphWithZeroCoverage() {
        expectedMatrix = new int[3][3];
        expectedMatrix[0][0] = -1;
        expectedMatrix[0][1] = -1;
        expectedMatrix[0][2] = -1;
        expectedMatrix[1][0] = -1;
        expectedMatrix[1][1] = 0;
        expectedMatrix[1][2] = -1;
        expectedMatrix[2][0] = -1;
        expectedMatrix[2][1] = -1;
        expectedMatrix[2][2] = -1;
        boardLength = 3;
        boardWidth = 3;
        initialColCoordinate = 1;
        initialRowCoordinate = 1;
        chessBoardDetails = new ChessBoardDetails(boardLength, boardWidth);
        int distanceMatrix[][] = minDistanceGraph.minStepToReachTarget(initialRowCoordinate, initialColCoordinate, boardLength, boardWidth, chessBoardDetails);
        for (int i = 0; i < boardWidth; i++) {
            Assert.assertArrayEquals(expectedMatrix[i], distanceMatrix[i]);
        }
    }

    /**
     * Test case with knight's position such that few cells are reachable
     */
    @Test
    public void testDistanceGraphWithPartialCoverage() {
        expectedMatrix = new int[3][3];
        expectedMatrix[0][0] = 2;
        expectedMatrix[0][1] = 1;
        expectedMatrix[0][2] = 4;
        expectedMatrix[1][0] = 3;
        expectedMatrix[1][1] = -1;
        expectedMatrix[1][2] = 1;
        expectedMatrix[2][0] = 0;
        expectedMatrix[2][1] = 3;
        expectedMatrix[2][2] = 2;
        boardLength = 3;
        boardWidth = 3;
        initialColCoordinate = 0;
        initialRowCoordinate = 2;
        chessBoardDetails = new ChessBoardDetails(boardLength, boardWidth);
        int distanceMatrix[][] = minDistanceGraph.minStepToReachTarget(initialRowCoordinate, initialColCoordinate, boardLength, boardWidth, chessBoardDetails);
        for (int i = 0; i < boardWidth; i++) {
            Assert.assertArrayEquals(expectedMatrix[i], distanceMatrix[i]);
        }
    }
}
