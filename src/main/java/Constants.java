/**
 * Class to maintain constant values.
 * Can be move to a configuration file if it requires frequent change of values.
 *
 */

public class Constants {
    public static final int SMALLER_STEP = 1;
    public static final int LARGER_STEP = 2;
    public static final int INITIAL_DISTANCE = 0;
}
