
/**
 * Main class that takes input values as console arguments.
 * As per the problem x coordinate represents column values and y coordinate represents row values.
 */
public class Knight {

    private static ChessBoardDetails chessBoardDetails;

    public static void main(String[] args) {
        int boardLength = Integer.parseInt(args[0]);
        int boardWidth = Integer.parseInt(args[1]);
        //initial position of the knight
        int initialColCoordinate = Integer.parseInt(args[2]); // x-coordinate
        int initialRowCoordinate = Integer.parseInt(args[3]); // y-coordinate

        try {
            chessBoardDetails = new ChessBoardDetails(boardLength, boardWidth);
            int distanceMatrix[][] = MinDistanceGraph
                    .minStepToReachTarget(initialRowCoordinate,
                            initialColCoordinate,
                            boardLength,
                            boardWidth,
                            chessBoardDetails);

            printDistanceGraph(boardLength, boardWidth, distanceMatrix);
        } catch (IllegalArgumentException ex) {
            System.out.println(ex.getLocalizedMessage());
        }
    }

    private static void printDistanceGraph(int boardLength, int boardWidth, int[][] distanceMatrix) {
        for(int i = 0; i < boardWidth; i++) {
         for(int j = 0; j < boardLength; j++) {
             System.out.print("\t" + distanceMatrix[i][j]);
         }
            System.out.println();
        }
    }
}