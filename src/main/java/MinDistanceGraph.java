import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * To get the min moves required for knight to cover all positions from the specified initial position
 */

public class MinDistanceGraph {

    /**
     * Method to get the distance matrix for the knight covering all possible position in minimum moves
     * @param knightPosRow0 Initial row position of the knight
     * @param knightPosCol0 Initial column position of the knight
     * @param columnSize Matrix/Chess board column size
     * @param rowSize Matrix/Chess board row size
     * @param chessBoardDetails Object specifying details of chess board
     * @return
     */
    public static int[][] minStepToReachTarget(int knightPosRow0, int knightPosCol0,
                                               int columnSize, int rowSize, ChessBoardDetails chessBoardDetails) {

        //  x and y directional offsets, where a knight can move from a certain position
        List<KnightMoveOffset> knightMoveOffsetList = KnightMoveOffset.getMoveOffsetsList();

        //  queue for storing states of the knight on the board
        Queue<Cell> cellQueue = new LinkedList<Cell>();

        //  push starting position of knight with 0
        Cell initialCellPos = new Cell(knightPosRow0, knightPosCol0, Constants.INITIAL_DISTANCE);
        cellQueue.add(initialCellPos);

        Cell newCell;
        int x, y;
        int chessBoardMatrix[][] = new int[rowSize][columnSize];
        boolean visited[][] = new boolean[rowSize][columnSize];
        for (int i = 0; i < rowSize; ++i) {
            Arrays.fill(chessBoardMatrix[i], -1);
        }

        // mark initial state as visited, as the knight starts from there
        visited[knightPosRow0][knightPosCol0] = true;

        //  loop for all elements in queue
        while (!cellQueue.isEmpty()) {
            newCell = cellQueue.element();
            cellQueue.remove();
            visited[newCell.getRowCoordinate()][newCell.getColCoordinate()] = true;

            // if current cell is equal to initial position, set the distance as 0 in the resultant distance matrix
            // else get the distance from the cell that is dequeued and store in the distance matrix
            //System.out.println("COORD: " + newCell.getRowCoordinate() + newCell.getColCoordinate());
            if (newCell.getRowCoordinate() == initialCellPos.getRowCoordinate() && newCell.getColCoordinate() == initialCellPos.getColCoordinate()) {
                chessBoardMatrix[newCell.getRowCoordinate()][newCell.getColCoordinate()] = 0;
            } else {
                chessBoardMatrix[newCell.getRowCoordinate()][newCell.getColCoordinate()] = newCell.getDistance();
            }

            //  loop for all reachable states
            for (int i = 0; i < knightMoveOffsetList.size(); i++) {
                KnightMoveOffset offsetCoordinates = knightMoveOffsetList.get(i);

                x = newCell.getRowCoordinate() + offsetCoordinates.getRowOffset();
                y = newCell.getColCoordinate() + offsetCoordinates.getColOffset();

                // If reachable state is not yet visited and
                // inside board, push that state into queue
                if (chessBoardDetails.isPositionOnChessBoard(x, y) && !visited[x][y])
                    cellQueue.add(new Cell(x, y, newCell.getDistance() + 1));

            }
        }
        return chessBoardMatrix;
    }
}
