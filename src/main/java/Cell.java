/**
 * Represents a matrix/chess board cell or square -
 * with coordinates and distance from knight's initial position of each square
 * row ~ y-axis coordinates; column ~ x-axis coordinates as per the problem statement
 *
 */

public class Cell {
    private final int rowCoordinate;
    private final int colCoordinate;
    private int distance;

    public Cell(int x, int y, int dist) {
        distance = dist;
        rowCoordinate = x;
        colCoordinate = y;
    }

    public int getRowCoordinate() {
        return rowCoordinate;
    }

    public int getColCoordinate() {
        return colCoordinate;
    }

    public int getDistance() {
        return distance;
    }

    @Override
    public String toString() {
        return "Cell{" +
                "rowCoordinate=" + rowCoordinate +
                ", colCoordinate=" + colCoordinate +
                ", distance=" + distance +
                '}';
    }
}