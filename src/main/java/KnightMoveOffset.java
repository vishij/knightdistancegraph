import java.util.ArrayList;
import java.util.List;

/**
 * Class to specify the offset with which the knight moves, which can be (+-2,+-1) or (+-1, +-2)
 */
public class KnightMoveOffset {
    private int rowOffset;
    private int colOffset;
    private static final List<KnightMoveOffset> knightMoveOffsetList;

    static {
        knightMoveOffsetList = new ArrayList<KnightMoveOffset>();
        knightMoveOffsetList.add(new KnightMoveOffset(-1, -2));
        knightMoveOffsetList.add(new KnightMoveOffset(-1, 2));
        knightMoveOffsetList.add(new KnightMoveOffset(-2, -1));
        knightMoveOffsetList.add(new KnightMoveOffset(-2, 1));
        knightMoveOffsetList.add(new KnightMoveOffset(1, -2));
        knightMoveOffsetList.add(new KnightMoveOffset(1, 2));
        knightMoveOffsetList.add(new KnightMoveOffset(2, -1));
        knightMoveOffsetList.add(new KnightMoveOffset(2, 1));
    }

    public KnightMoveOffset(int row, int column) {
        this.rowOffset = row;
        this.colOffset = column;
    }

    public int getRowOffset() {
        return rowOffset;
    }

    public int getColOffset() {
        return colOffset;
    }

    public static List<KnightMoveOffset> getMoveOffsetsList() {
        return knightMoveOffsetList;
    }
}

