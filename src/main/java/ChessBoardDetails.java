
/**
 * Chessboard object specifying dimensions
 */
public class ChessBoardDetails {
    private final int columnSize;
    private final int rowSize;

    /**
     * When knight moves , it moves 2 squares and then 1 square in L-shape.
     *
     * @param chessBoardLen board length
     * @param chessBoardWid board width
     */
    public ChessBoardDetails(int chessBoardLen, int chessBoardWid) {
        if (chessBoardLen > 0 && chessBoardWid > 0) {
            columnSize = chessBoardLen;
            rowSize = chessBoardWid;
        } else {
            throw new IllegalArgumentException("Invalid Input! Chess board length and width should be greater than zero.");
        }
    }

    /**
     * @return true if the position is within bounds of chessboard
     */
    public boolean isPositionOnChessBoard(int row, int column) {
        return (row < rowSize && column < columnSize
                && row >= 0 && column >= 0);
    }
}